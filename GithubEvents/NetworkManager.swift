import Foundation
import UIKit

class NetworkManager {
    
    static let shared = NetworkManager()
    var array: [Profile] = []
    var objects = [Objects?]()
    
    private init() {}
    
    func getProfiles(completion: @escaping (([Profile]) -> ())) {
        var objects = [Objects?]()
        guard let urlComponents = URLComponents(string: "https://api.github.com/events?page=1") else {return}
        
        var request = URLRequest(url: urlComponents.url!)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error == nil, let data = data {
                do {
                    objects = try JSONDecoder().decode([Objects].self, from: data)
                    self.objects = objects
                    self.createList()
                    completion(self.array)
                } catch {
                    print(error)
                }
            }
        }
        task.resume()
    }
    
    func createList() {
        for object in objects {
            if let object = object {
                guard let eventType = object.type else { return }
                guard let eventDate = object.created_at else { return }
                if let actor = object.actor {
                    guard let userName = actor.login else { return }
                    guard let userImage = actor.avatar_url else { return }
                    
                    if let repository = object.repo {
                        guard let repositaryName = repository.name else { return }
                        
                        let newArray = Profile(eventType: eventType, userName: userName, userImage: userImage, repositoryName: repositaryName, eventDate: eventDate)
                        array.append(newArray)
                    }
                }
            }
        }
    }
}
