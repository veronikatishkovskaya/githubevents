import UIKit

class CustomTableViewCell: UITableViewCell {
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var eventType: UILabel!
    @IBOutlet weak var authorName: UILabel!
    @IBOutlet weak var eventDate: UILabel!
    
    var imageCache = ImageCache()
    
    func configure(object: Profile) {
        eventType.text = object.eventType
        authorName.text = object.userName
        eventDate.text = object.eventDate
        
        let image = object.userImage
        if let imageUrl = URL(string: image) {
            imageCache.downloadImage(url: imageUrl) { image in
                DispatchQueue.main.async {
                    self.userImage.image = image
                }
            }
        }
    }
}
