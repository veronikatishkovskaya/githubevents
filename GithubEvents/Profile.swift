import Foundation

class Profile {
    var eventType: String
    var userName: String
    var userImage: String
    var repositoryName: String
    var eventDate: String
    
    init(eventType: String, userName: String, userImage: String, repositoryName: String, eventDate: String) {
        self.eventType = eventType
        self.userName = userName
        self.userImage = userImage
        self.repositoryName = repositoryName
        self.eventDate = eventDate
    }
}
