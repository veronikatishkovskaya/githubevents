import Foundation

class Objects: Codable {
    var type: String?
    var actor: User?
    var repo: Repository?
    var created_at: String?
}

class User: Codable {
    var login: String?
    var avatar_url: String?
}

class Repository: Codable {
    var name: String?
}
