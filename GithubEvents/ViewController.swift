import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var manager = NetworkManager.shared
    var profilesArray = [Profile]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getProfiles()
    }
    
    func getProfiles() {
        self.manager.getProfiles { (data) in
            DispatchQueue.main.async {
                self.startLoading()
                self.profilesArray.append(contentsOf: data)
                self.tableView.reloadData()
                self.finishLoading()
            }
        }
    }
    
    func startLoading() {
        DispatchQueue.main.async {
            self.activityIndicator.startAnimating()
            self.view.addSubview(self.activityIndicator)
        }
    }
    
    func finishLoading() {
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
            self.activityIndicator.hidesWhenStopped = true
        }
    }
    
}
extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return profilesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CustomTableViewCell", for: indexPath) as? CustomTableViewCell else {
            return UITableViewCell()
        }
        cell.configure(object: profilesArray[indexPath.row])
        cell.backgroundColor = .clear
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let detailScreen = storyboard?.instantiateViewController(withIdentifier: "DetailScreen") as? DetailScreen else {return}
        detailScreen.detailArray.append(profilesArray[indexPath.row])
        navigationController?.pushViewController(detailScreen, animated: true)
    }
}
