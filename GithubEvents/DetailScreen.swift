import Foundation
import UIKit

class DetailScreen: UIViewController {
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var repositoryName: UILabel!
    @IBOutlet weak var authorName: UILabel!
    
    var detailArray = [Profile]()
    var imageCache = ImageCache()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getDetails()
    }
    
    @IBAction func goBackToViewController(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    func getDetails() {
        for info in detailArray {
            let image = info.userImage
            if let imageUrl = URL(string: image) {
                imageCache.downloadImage(url: imageUrl) { image in
                    DispatchQueue.main.async {
                        self.userImage.image = image
                        self.userImage.contentMode = .scaleAspectFill
                    }
                }
            }
            
            let fullName = info.repositoryName
            let repository = fullName.components(separatedBy: "/")
            repositoryName.text = "Repository: \(repository[1])"
            authorName.text = "Author: \(info.userName)"
        }
    }
}
